-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Client :  localhost
-- Généré le :  Jeu 19 Mars 2020 à 11:03
-- Version du serveur :  10.1.44-MariaDB-1~jessie
-- Version de PHP :  7.0.33-1~dotdeb+8.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `previoo2`
--

-- --------------------------------------------------------

--
-- Structure de la table `previoo_electeurs`
--

CREATE TABLE `previoo_electeurs` (
  `id_electeur` int(11) NOT NULL,
  `dt_crea_electeur` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `genre_electeur` tinyint(4) DEFAULT NULL,
  `nom_electeur` varchar(100) NOT NULL,
  `prenom_electeur` varchar(100) NOT NULL,
  `norue_electeur` int(11) DEFAULT NULL,
  `complementrue_electeur` varchar(20) DEFAULT NULL,
  `adresse_electeur` varchar(255) DEFAULT NULL,
  `adresse2_electeur` varchar(255) NOT NULL DEFAULT '',
  `cp_electeur` varchar(20) DEFAULT NULL,
  `ville_electeur` varchar(150) NOT NULL,
  `pays_electeur` varchar(200) NOT NULL DEFAULT 'FRANCE',
  `dt_naissance_electeur` date DEFAULT NULL,
  `tel_electeur` varchar(200) DEFAULT NULL,
  `portable_electeur` varchar(20) DEFAULT NULL,
  `email_electeur` varchar(200) DEFAULT NULL,
  `id_bv_electeur` varchar(255) DEFAULT NULL,
  `abstentionniste_electeur` tinyint(4) DEFAULT NULL COMMENT 'NULL = inconnu , 0 = non, 1 = oui'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `previoo_electeurs`
--

INSERT INTO `previoo_electeurs` (`id_electeur`, `dt_crea_electeur`, `genre_electeur`, `nom_electeur`, `prenom_electeur`, `norue_electeur`, `complementrue_electeur`, `adresse_electeur`, `adresse2_electeur`, `cp_electeur`, `ville_electeur`, `pays_electeur`, `dt_naissance_electeur`, `tel_electeur`, `portable_electeur`, `email_electeur`, `id_bv_electeur`, `abstentionniste_electeur`) VALUES
(1, '2020-03-19 09:47:00', 2, 'DURAND', 'Jeanne', 12, '', 'RUE DES FLEURS', '', '12345', 'VILLE', 'FRANCE', '1980-05-12', '0220202020', '0123456789', 'exemple@exemple.fr', NULL, 0),
(2, '2020-03-19 09:47:00', 2, 'MARTIN', 'Claire', 6, 'BIS', 'RUE DE LA CHARRETTE', '', '12345', 'VILLE', 'FRANCE', '1968-03-25', '0210101010', '0678912345', 'exemple@exemple.fr', NULL, 0),
(3, '2020-03-19 09:47:00', 1, 'CERCLE', 'Georges', 33, '', 'PLACE DE L\'ÉGLISE', '', '', 'VILLE', 'FRANCE', '1974-01-02', '0230303030', '0123456788', 'exemple@exemple.fr', NULL, 1),
(4, '2020-03-19 09:47:00', 2, 'SIMONET', 'Marie', 15, '', 'RUE DES ÉCOLES', 'BATIMENT A - APT 136', '12345', 'VILLE', 'FRANCE', '1985-02-11', '', '0123456789', 'exemple@exemple.fr', NULL, 1),
(5, '2020-03-19 09:47:00', 1, 'DORTIN', 'Rémi', 63, 'TER', 'AVENUE PRINCIPALE', '', '12346', 'VILLAGE', 'FRANCE', '1985-02-12', '', '', 'exemple@exemple.fr', NULL, 0),
(6, '2020-03-19 09:47:00', 2, 'DUPUIS', 'Josette', 1, '', 'CHEMIN DU SOLEIL', '', '12346', 'VILLAGE', 'FRANCE', '1985-02-13', '', '', 'exemple@exemple.fr', NULL, 1),
(7, '2020-03-19 09:47:00', 1, 'LACROIX', 'Jean-Michel', 5, '', 'RUE LÉVIATHAN', '', '12347', 'LEBOIS', 'FRANCE', '1995-08-04', '', '0601020304', 'lacroix@yopmail.com', NULL, 0),
(8, '2020-03-19 09:47:00', 2, 'LACROIX', 'Michelle', 5, '', 'RUE LÉVIATHAN', '', '12347', 'LEBOIS', 'FRANCE', '1985-08-04', '', '0601020304', 'lacroix@yopmail.com', NULL, 0),
(9, '2020-03-19 09:47:00', 1, 'TIBRE', 'Claude', 2, '', 'RUE CHARLES DE GAULLE', '', '12347', 'LEBOIS', 'FRANCE', '1975-07-03', '', '0601020304', 'tibre@yopmail.com', NULL, 0),
(10, '2020-03-19 09:47:00', 2, 'YVES', 'Marie', 1, '', 'RUE CHARLES DE GAULLE', '', '12347', 'LEBOIS', 'FRANCE', '1995-02-01', '', '0601020304', 'yves@yopmail.com', NULL, 1),
(11, '2020-03-19 09:47:00', 1, 'DUPONT', 'Jean', 3, '', 'RUE LÉVIATHAN', '', '12347', 'LEBOIS', 'FRANCE', '1985-09-01', '', '0601020304', 'lacroix@yopmail.com', NULL, 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `previoo_electeurs`
--
ALTER TABLE `previoo_electeurs`
  ADD PRIMARY KEY (`id_electeur`),
  ADD KEY `three_column_index` (`nom_electeur`,`prenom_electeur`,`dt_naissance_electeur`),
  ADD KEY `two_column_index` (`nom_electeur`,`prenom_electeur`),
  ADD KEY `nom_electeur` (`nom_electeur`),
  ADD KEY `prenom_electeur` (`prenom_electeur`),
  ADD KEY `dt_naissance_electeur` (`dt_naissance_electeur`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `previoo_electeurs`
--
ALTER TABLE `previoo_electeurs`
  MODIFY `id_electeur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
