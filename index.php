<?php

require_once "previoo/previoo.php";

use previoo\Configuration;
/**
 * en cas de modification des routes, mettre a jour l'autoloader
 * php composer.phar update
 */
Configuration::setConfigurationFile('application/configuration.ini');

define('APP_FOLDER', 'application');
define('TEMPLATE_FOLDER', APP_FOLDER . '/templates');
define('VIEW_FOLDER', APP_FOLDER . '/views');
define('RESOURCE_FOLDER', APP_FOLDER . '/resources');
define('APP_NAMESPACE', 'app');

$app = \previoo\Application::getInstance();
$app->setdefaultControllerName('electeur');
$app->run();
