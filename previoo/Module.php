<?php

namespace previoo;

interface Module
{
    // constructeur par défaut d'un Module
    public function render();
}
