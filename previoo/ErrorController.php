<?php

namespace previoo;

class ErrorController extends Controller
{
    // page a afficher en cas d'erreur en mode production
    public function getDefaultActionName()
    {
        return 'error';
    }

    public function errorAction()
    {
        header('HTTP/1.1 500. Internal Server Error', true, 500);
        die();
    }
}
