<?php

namespace previoo;

//require_once "configuration.php";

class Database
{
    private static $_pdo = null;

    private function __construct()
    {
        $conf = Configuration::getInstance();

        // choix de la base de donnée utiliser
        if (!isset($conf->db_driver)) {
            throw new Error("Database : driver non renseigné");
        }
        switch ($conf->db_driver) {
            case 'pdo_mysql':
                $this->makePDOMySQL($conf);
                break;

            case 'pdo_sqlite':
                $this->makePDOSQLite($conf);
                break;

            default:
                throw new Error("Database : driver non pris en compte");
        }

        self::$_pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        self::$_pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
    }

       // création d'une instance de la basse de donnée
       public static function getInstance()
       {
           if (is_null(self::$_pdo)) {
               new Database();
           }
           return self::$_pdo;
       }

    // connexion a une base de donnée MySql
    private function makePDOMySQL(Configuration $conf)
    {
        $expectedKeys = ['db_host', 'db_login', 'db_password', 'db_base'];
        foreach ($expectedKeys as $key) {
            if (!isset($conf->$key)) {
                throw new Error("Database : $key manquant dans le fichier ini");
            }
        }
        try {
            self::$_pdo = new \PDO(
                "mysql:host={$conf->db_host};dbname={$conf->db_base};charset=utf8",
                $conf->db_login,
                $conf->db_password
            );
        } catch (\PDOException $ex) {
            throw new Error("Database : erreur connexion MySQL " . $ex->getMessage());
        }
    }


    // connexion a une base de donnée SqlLite
    private function makePDOSQLite(Configuration $conf)
    {
        if (!isset($conf->db_file)) {
            throw new Error("Erreur ini : db_file manquant");
        }
        try {
            self::$_pdo = new \PDO("sqlite:{$conf->db_file}");
        } catch (\PDOException $ex) {
            throw new Error("Erreur connexion MySQL" . $ex->getMessage());
        }
    }

 
}
