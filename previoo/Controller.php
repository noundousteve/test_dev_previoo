<?php

namespace previoo;

abstract class Controller
{

    public function getDefaultActionName()
    {
        throw new Error("Controlleur : aucune action par défaut n'a été définie pour la classe " . get_class($this));
    }

    // exécution de d'une action
    public function execute($actionName)
    {
        $actionMethod = $actionName . 'Action';
        if (!\method_exists($this, $actionMethod)) {
            throw new Error("Controller : action non disponible " . $actionName . " pour le controleur " . \get_class($this));
        }
        $this->$actionMethod();
    }
}
