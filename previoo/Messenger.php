<?php

namespace previoo;

abstract class Messenger
{
    const SESSION_KEY = 'previoo.messenger';

    // sauvegarder un message dans une session
    public static function setMessage($message)
    {
        $_SESSION[self::SESSION_KEY] = $message;
    }

    // recupération d'un message sauvegarder 
    public static function getMessage()
    {
        if (!isset($_SESSION[self::SESSION_KEY])) return false;
        $message = $_SESSION[self::SESSION_KEY];

        // efface le message après lecture
        unset($_SESSION[self::SESSION_KEY]);
        return $message;
    }
}
