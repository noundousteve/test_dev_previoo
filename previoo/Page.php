<?php

namespace previoo;

class Page
{
    private static $_instance = null;
    protected $viewFile = null;
    protected $templateFile = null;
    // protected $moduleClass = null;
    protected $data = [];

    private function __construct()
    { }

     // création d'une instance de la page
     public static function getInstance()
     {
         if (is_null(self::$_instance)) {
             self::$_instance = new Page();
         }
         return self::$_instance;
     }

       // mapage de la view a un template
       public function init($template, $view)
       {
           $this->setTemplate($template);
           $this->setView($view);
       }

      // recupération du template
      public function setTemplate($template)
      {
          $templateFile = TEMPLATE_FOLDER . '/' . $template . '.template.php';
          if (!is_readable($templateFile)) {
              throw new Error('Page : fichier de template introuvable ' . $template);
          }
          $this->templateFile = $templateFile;
          return $this;
      }

    // chargement du template
    public function render()
    {
        if (is_null($this->templateFile)) {
            throw new Error('Page: aucun template remseigné');
        }
        require $this->templateFile;
    }

       // recupération d'une view
       public function setView($view)
       {
           $viewFile = VIEW_FOLDER . '/' . $view .'/'. $view . '.view.php';
           if (!is_readable($viewFile)) {
               throw new Error('Page : fichier de vue introuvable ' . $view);
           }
           $this->viewFile = $viewFile;
           return $this;
       }

    // chargement d'une view
    public function insertView()
    {
        if (is_null($this->viewFile)) {
            throw new Error('Page : aucune view renseignée');
        }
        require $this->viewFile;
    }

    // Récupération et chargement d'un module
    public static function insertModule($module)
    {
        $moduleClass = APP_NAMESPACE . '\\modules\\' . $module;
        if (!class_exists($moduleClass)) {
            throw new Error('Page : aucune classe ne corrrespond au module ' . $moduleClass);
        }
        $interfaces = \class_implements($moduleClass);
        if (!isset($interfaces['previoo\Module'])) {
            throw new Error('Page : la classe ' . $module . 'ne respecte pas l\'interface');
        }
        $objModule = new $moduleClass();
        $objModule->render();
    }

    public function __get($item)
    {
        if (!isset($this->data[$item])) {
            throw new Error("Page : clé inexistante " . $item);
        }
        return $this->data[$item];
    }

    public function __isset($item)
    {
        return isset($this->data[$item]);
    }

    public function __set($item, $value)
    {
        $this->data[$item] = $value;
    }    
}
