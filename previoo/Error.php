<?php

namespace previoo;

class Error extends \Exception
{
    protected $renderFile;
    public function  __construct($message)
    {
        parent::__construct($message);
        $this->renderFile = 'html/error.html.php';
    }

    public function render()
    {
        $app = Application::getInstance();
        $logger = $app->getLogger();

        // recupération des information dans les logs
        $logger->error($this->message, array(
            'file' => $this->getLine(),
            'line' => $this->getLine()
        ));


        switch ($app->getRunMode()) {
            case Application::DEBUG_MODE:
                $this->debugMondeRender();
                break;
            default:
                $this->productionModeRender();
                break;
        }
    }

    /**
     * en mode production toutes les erreurs retourne le controlleur errorController.php
     */
    private function productionModeRender()
    {
        Application::redirect('?controller=error');
    }

    /**
     * en mode debug on renvoie des informations sur l'erreur
     */
    private function debugMondeRender()
    {
        $trace = $this->getTrace();
        $file = $this->getFile();
        $line = $this->getLine();
        $function = $trace[0]['function'] . '()';
        include $this->renderFile;
        die();
    }
}
