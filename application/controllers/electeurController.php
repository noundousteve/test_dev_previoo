<?php

namespace app\controllers;

use previoo\Controller;
use previoo\Page;
use app\models\electeurModel;
use previoo\Application;
use previoo\Messenger;
use previoo\Error;

class electeurController extends Controller
{

    public function getDefaultActionName()
    {
        return 'lister';
    }


    public function listerAction()
    {

        $page = Page::getInstance(); 
        $page->init('electeur', 'electeur');
        $page->electeurs = electeurModel::getAllElecteur();
    }

    public function ajouterAction()
    {
        // Réglage de base 
        $page = Page::getInstance();
        $page->init('electeur', 'electeur-form'); 

        // Chargement du JSON des pays et villes du monde
        try{
            $json = file_get_contents("application/resources/country-state.json");  
            $page->country_states = json_decode($json,true); 
        }catch(Exception $e ){ 
            throw new Error("Exception reçue : ',  $e->getMessage(),");
        }

        // Préparation des données
        $page->_formMessage = "";  
        $page->nom_electeur = "";
        $page->prenom_electeur = ""; 
        $page->norue_electeur = "";
        $page->complementrue_electeur = "";
        $page->adresse_electeur = "";
        $page->adresse2_electeur = "";
        $page->cp_electeur = "";
        $page->dt_naissance_electeur = "";
        $page->tel_electeur = "";
        $page->portable_electeur = "";
        $page->email_electeur = "";   

        // Chager les villes en fonction du pays sélectionner 
    if(!empty($_POST["pays_electeur"]) AND empty($_POST["submit"])) :?>
        <option value="">--- Sélectionnez une ville ---</option>
   <?php 
        $id_ville = $_POST["pays_electeur"]; 
        foreach ($page->country_states[$id_ville]["states"] as $value) : ?>
                                <option name="option" value=<?php echo $value ?> ><?php echo $value ?></option>            
        <?php endforeach; ?> 
   <?php endif; 
   ?> 


   <?php 
      
        // si le formulaire n'est pas envoyé
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            return;
        }

        // Récuperation des données; 
        $page->genre_electeur =  filter_input(INPUT_POST, 'genre_electeur');
        /*
        if (!isset($page->genre_electeur)) {
            $page->_formMessage = "Erreur : Veuillez choisir un genre";
            return;
        }
        */  
        
        $page->nom_electeur = filter_input(INPUT_POST, 'nom_electeur');
        if (trim($page->nom_electeur) === "") {
            $page->_formMessage = "Erreur : Veuillez remplir le champ nom";
            return;
        }

        $page->prenom_electeur = filter_input(INPUT_POST, 'prenom_electeur');
        if (trim($page->prenom_electeur) === "") {
            $page->_formMessage = "Erreur : Veuillez remplir le champ prenom";
            return;
        }

        $page->norue_electeur = filter_input(INPUT_POST, 'norue_electeur');
        if (trim($page->norue_electeur) === "" || trim($page->norue_electeur) < 0 ) {
            $page->_formMessage = "Erreur : Veuillez remplir le champ numéro de rue";
            return;
        }

        $page->adresse_electeur = filter_input(INPUT_POST, 'adresse_electeur');
        if (trim($page->adresse_electeur) === "") {
            $page->_formMessage = "Erreur : Veuillez remplir le champ adresse";
            return;
        }

        $page->cp_electeur = filter_input(INPUT_POST, 'cp_electeur');
        if (trim($page->cp_electeur) === "") {
            $page->_formMessage = "Erreur : Veuillez remplir le champ code postal";
            return;
        }

        $page->pays_electeur = filter_input(INPUT_POST, 'pays_electeur');
        if (trim($page->pays_electeur) === "") {
            $page->_formMessage = "Erreur : Veuillez remplir le champ pays";
            return;
        }

        
        $page->ville_electeur = filter_input(INPUT_POST, 'ville_electeur');
        if (trim($page->ville_electeur) === "") {
            $page->_formMessage = "Erreur : Veuillez remplir le champ ville";
            return;
        }


        $page->dt_naissance_electeur = filter_input(INPUT_POST, 'dt_naissance_electeur');
        if (trim($page->dt_naissance_electeur) === "") {
            $page->_formMessage = "Erreur : Veuillez remplir le champ date de naissance";
            return;
        }

        if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i", $_POST['email_electeur']))
        {
            $page->email_electeur = filter_input(INPUT_POST, 'email_electeur');
        }
      else
        {
            $page->email_electeur = "";
        }

        if (trim($page->email_electeur) === "") {
            $page->_formMessage = "Erreur : Veuillez remplir le champ email";
            return;
        }
        
        $page->abstentionniste_electeur = filter_input(INPUT_POST, 'abstentionniste_electeur');
        if (trim($page->abstentionniste_electeur) === "") {
            $page->_formMessage = "Erreur : Veuillez remplir le champ ville";
            return;
        }
 

        
        // Enregistrement 
        $id_ville = $page->pays_electeur;
        electeurModel::insertElecteur([
            'genre_electeur' => $page->genre_electeur,
            'nom_electeur' => $page->nom_electeur,
            'prenom_electeur' => $page->prenom_electeur,
            'norue_electeur' => $page->norue_electeur,
            'complementrue_electeur' => $page->complementrue_electeur,
            'adresse_electeur' => strtoupper($page->adresse_electeur),
            'adresse2_electeur' => strtoupper($page->adresse2_electeur),
            'cp_electeur' => $page->cp_electeur,
            'pays_electeur' =>strtoupper($page->country_states[$id_ville]["name"]),
            'ville_electeur' => strtoupper($page->ville_electeur) ,
            'dt_naissance_electeur' => $page->dt_naissance_electeur,
            'tel_electeur' => $page->tel_electeur,
            'portable_electeur' => $page->portable_electeur,
            'email_electeur' => $page->email_electeur,
            'abstentionniste_electeur' => $page->abstentionniste_electeur 
        ]);
        Messenger::setMessage("Electeur enregistrer avec succès !!!");
        Application::redirect('?controller=electeur&action=ajouter');

    }
}
