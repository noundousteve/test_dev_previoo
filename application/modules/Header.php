<?php

namespace app\modules;

class Header implements \previoo\Module
{
    private static $items = [
        [
            "title" => "LISTER",
            "url" => "?controller=electeur&action=lister"
        ],
        [
            "title" => "AJOUTER",
            "url" => "?controller=electeur&action=ajouter"
        ]
    ];
    public function render()
    {
        ?>
        
        <nav class="navbar navbar-expand-lg navbar-dark ">
            
            <a class="navbar-brand" href="#" title="Previoo">
            <img src="https://www.previoo.com/website/img/logo.png" alt="Previoo"> 
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
            
            <span class="navbar-text mr-auto">
                    Electeurs
                </span>
                <ul class="navbar-nav">

                    <?php foreach (self::$items as $M) : ?>
                        <li class="nav-item">
                            <a class="nav-link" OnClick="onFocus(this)" href="<?php echo $M['url']; ?>">
                                <?php echo $M['title']; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </nav>
                
 
<?php
    }
}
