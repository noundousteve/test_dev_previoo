<?php

namespace app\modules;

class Footer implements \previoo\Module
{ 
    public function render()
    {
        ?>
    
    <div class="row justify-content-center">  
            <img class="mb-2" src="../../assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">
          <div class="col-6 col-md">
            <h5>Previoo</h5>
            <ul class="list-unstyled text-small"> 
                            <li><a href="http://www.previoo.com/#fonc" title="Fonctionnalités">Fonctionnalités</a></li>
                            <li><a href="http://www.previoo.com/#tarif" title="Tarifs">Tarifs</a></li>
                            <li><a href="http://app.previoo.com/" title="Connexion">Connexion</a></li>
							<li><a href="https://previoo.com/previoo-rgpd.php" target="_blank" title="PREVIOO et le RGPD">RGPD</a></li>
                        
            </ul>
          </div>

          <div class="col-6 col-md">
            <h5>Communauté</h5>
            <ul class="list-unstyled text-small"> 
                            <li><a href="http://www.plebiscit.fr/blog/" target="_blank" title="Blog">Blog</a></li>
                            <li><a href="https://www.youtube.com/user/AgencePlebiscit" target="_blank" title="Youtube">Youtube</a></li>
                            <li><a href="https://twitter.com/plebiscit" target="_blank" title="Twitter">Twitter</a></li>
							<li><a href="https://previoo.com/newsletter.php" target="_blank" title="Newsletter">Newsletter</a></li>
                        </ul>
          </div>

          <div class="col-6 col-md">
            <h5>à propos</h5>
            <ul class="list-unstyled text-small"> 
                            <li><a href="http://www.plebiscit.fr/" target="_blank" title="Plebiscit">Plebiscit</a></li>
                            <li><a href="/website/pdf/CGU-PREVIOO.pdf" target="_blank" title="CGU - CGV">CGU - CGV</a></li>
                            <li><a href="javascript:void(0);" data-featherlight="#legal-msg"  title="Mentions légales">Mentions légales</a></li>
							<li><a href="https://previoo.com/contact.php" target="_blank" title="Contactez nous">Contactez nous</a></li>
                        </ul>
          </div>

          
          <div class="col-6 col-md">
            <h5>Previoo</h5>
            <ul class="list-unstyled text-small"> 
                            <li class="footer-coord">Un service édité par Plebiscit<br>96 rue de la Muse - BP 92 061<br>17 010 LA ROCHELLE Cedex 1</li>
							<li><a href="mailto:hello@previoo.com">hello@previoo.com</a></li>
             </ul>
          </div>

        </div>

        <div class="row">
        <div class="col-12 col-md">
            <small class="d-block mb-3 text-center">Copyright Previoo 2019 ©</small>
            
          </div>
        </div>
<?php
    }
}
