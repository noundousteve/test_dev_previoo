<?php
namespace app\tests; 
 
use app\models\electeurModel;


  class electeurTest extends \PHPUnit\Framework\TestCase{

    use \PHPUnit\DbUnit\TestCaseTrait;
    
    // only instantiate pdo once for test clean-up/fixture load
    static private $pdo = null;

    // only instantiate PHPUnit\DbUnit\Database\Connection once per test
    private $conn = null;

    /**
     * @return PHPUnit\DbUnit\Database\Connection
     */
    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new \PDO( $GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD'] );
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    /**
     * @return PHPUnit\DbUnit\DataSet\IDataSet
     */ 
    
    public function getDataSet()
    {
        return $this->createMySQLXMLDataSet('application/tests/previoo.xml');
    }

    public function testCalculate()
    {
        $this->assertSame(2, 1 + 1);
    }
 

}