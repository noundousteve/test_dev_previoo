<?php

namespace app\models;

use previoo\Error;

class electeurModel
{
    public static function getAllElecteur()
    {
        $pdo = \previoo\Database::getInstance();

        $req = $pdo->prepare("SELECT * FROM previoo_electeurs");

        try { 
            $req->execute();
            $data = $req->fetchAll();
        } catch (\PDOException $ex) {
            throw new Error("Erreur SQL " . $ex->getMessage());
        }

        return $data;
    }

    

    public static function insertElecteur(array $data)
    {
        $expectedKeys = ['genre_electeur', 'nom_electeur', 'prenom_electeur','norue_electeur', 'adresse_electeur', 'ville_electeur','pays_electeur', 'dt_naissance_electeur', 'email_electeur','abstentionniste_electeur'];
        foreach ($expectedKeys as $key) {
            if (!isset($data[$key])) {
                throw new Error("$key manquant");
            }
        }
        $pdo = \previoo\Database::getInstance(); 

        $req = $pdo->prepare("INSERT INTO previoo_electeurs(genre_electeur,nom_electeur,prenom_electeur,norue_electeur,complementrue_electeur,adresse_electeur,adresse2_electeur,cp_electeur,ville_electeur,pays_electeur,dt_naissance_electeur,tel_electeur,portable_electeur,email_electeur,abstentionniste_electeur) 
        VALUES (:genre_electeur,:nom_electeur,:prenom_electeur,:norue_electeur,:complementrue_electeur,:adresse_electeur,:adresse2_electeur,:cp_electeur,:ville_electeur,:pays_electeur,:dt_naissance_electeur,:tel_electeur,:portable_electeur,:email_electeur,:abstentionniste_electeur)");

        try {
            $req->bindValue(':genre_electeur', $data['genre_electeur']);
            $req->bindValue(':nom_electeur', $data['nom_electeur']);
            $req->bindValue(':prenom_electeur', $data['prenom_electeur']);
            $req->bindValue(':norue_electeur', $data['norue_electeur']);
            $req->bindValue(':complementrue_electeur', (!empty($data['complementrue_electeur']))? $data['complementrue_electeur'] : ''  );
            $req->bindValue(':adresse_electeur', $data['adresse_electeur']);
            $req->bindValue(':adresse2_electeur', (!empty($data['adresse2_electeur'])) ? $data['adresse2_electeur'] : '');
            $req->bindValue(':cp_electeur', $data['cp_electeur']);
            $req->bindValue(':ville_electeur', $data['ville_electeur']);
            $req->bindValue(':pays_electeur', $data['pays_electeur']);
            $req->bindValue(':dt_naissance_electeur', $data['dt_naissance_electeur']);
            $req->bindValue(':tel_electeur',  (!empty($data['tel_electeur'])) ? $data['tel_electeur'] : '');
            $req->bindValue(':portable_electeur', (!empty($data['portable_electeur'])) ? $data['portable_electeur'] : '');
            $req->bindValue(':email_electeur', $data['email_electeur']); 
            $req->bindValue(':abstentionniste_electeur', $data['abstentionniste_electeur']);
            $req->execute();
        } catch (\PDOException $ex) {
            die("Erreur SQL " . $ex->getMessage());
        }
        return $pdo->lastInsertId();
    }
    
}
