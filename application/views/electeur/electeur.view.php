 <!-- CSS personaliser --> 
    <link rel="stylesheet" href="application\views\electeur\electeur.css">  

    <div style="height: 50px;"></div>
<div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">
          <table id="electeurs" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Pays</th>
                <th>Ville</th>
                <th>N° Rue</th> 
                <th>Adresse </th>
                <th>Telephones</th>
                <th>Adresse mail</th>
              </tr>
            </thead> 
            <tbody>
                    <?php foreach ($this->electeurs as $m) : ?>
                        <tr>

                            <td>
                                <?php echo utf8_encode($m["nom_electeur"]) ?>
                            </td>
                            <td><?php echo $m["prenom_electeur"] ?></td>
                            <td><?php echo $m["pays_electeur"] ?></td>
                            <td><?php echo $m["ville_electeur"] ?></td> 
                            <td><?php echo $m["norue_electeur"] ?></td> 
                            <td><?php 
                            if(!empty ($m["adresse_electeur"] )){
                              echo $m["adresse_electeur"] ;
                            }
                            if(!empty ($m["adresse2_electeur"] )){
                              echo ' ' . $m["adresse2_electeur"] ;
                            }
                             ?></td> 
                            <td><?php
                            if(!empty ($m["tel_electeur"] )){
                              echo 'tel: ' . $m["tel_electeur"] ;
                            }
                            if(!empty ($m["portable_electeur"] )){
                              echo ' Portable: ' . $m["portable_electeur"] ;
                            }
                            ?></td> 
                            <td><?php echo $m["email_electeur"] ?></td> 
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            <tfoot>
              <tr>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Pays</th>
                <th>Ville</th>
                <th>N° Rue</th> 
                <th>Adresse </th>
                <th>Telephones</th>
                <th>Adresse mail</th>
              </tr>
              </tr>
            </tfoot>
          </table> 
        </div>
      </div>
    </div>
  </div>

   <!-- JS personaliser --> 
  <script type="text/javascript" src="application\views\electeur\electeur.js" defer></script>

