
 <!-- CSS personaliser --> 
 <link rel="stylesheet" href="application\views\electeur-form\electeur-form.css"> 

 

<!-- recuperation du _formMessage en cas d' erreur --> 
<?php if (isset($this->_formMessage) & $this->_formMessage != "") : ?>
    <div class="alert alert-danger" role="alert">
        <p><?php echo $this->_formMessage; ?></p>
    </div>
<?php endif; ?>

<?php
// recuperation du message en cas de success
use previoo\Messenger;

$message = Messenger::getMessage();
if ($message !== false) :
    ?>
    <div class="alert alert-success" role="alert">
        <p><?php echo $message; ?></p>
    </div>

<?php endif; ?> 

<div style="height: 50px;"></div>


<div class="container">

    <div class="row">
        <div class="col-12">
        <div class="card" style="padding: 2rem;margin:2rem;">
        
        <div class="align-self-center">
                <h3>Enregistrement d'électeur</h3>
            </div>

            <form method="post">
                <span><sup>*</sup>Obligatoire</span>
                 <hr>

                 <div class="form-group">
                    <sup>*</sup> 
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="genre_electeur" id="homme" value="1">
                        <label class="form-check-label" for="homme">Mr</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="genre_electeur" id="femme" value="2">
                        <label class="form-check-label" for="femme">Mne</label> 
                    </div> 
                </div> 

                <div class="form-group">
                    <label for="nom_electeur">Nom <sup>*</sup> </label>
                    <input type="text" class="form-control" id="nom_electeur" name="nom_electeur" placeholder="Nom de l'electeur" value="<?php echo filter_var($this->nom_electeur, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
                </div>

                <div class="form-group">
                    <label for="prenom_electeur">Prénom <sup>*</sup>  </label>
                    <input type="text" class="form-control" id="prenom_electeur" name="prenom_electeur" placeholder="Prénom de l'électeur" value="<?php echo filter_var($this->prenom_electeur, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
                </div>

                <div class="form-group">
                    <label for="norue_electeur">Numéro de Rue <sup>*</sup></label>
                    <input type="number"  class="form-control" id="norue_electeur" name="norue_electeur" placeholder="Numéro de rue" value="<?php echo filter_var($this->norue_electeur, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
                </div>

                <div class="form-group">
                    <label for="complementrue_electeur">Complement de Rue</label>
                    <input type="text" class="form-control" id="complementrue_electeur" name="complementrue_electeur" placeholder="complement de rue de electeur" value="<?php echo filter_var($this->complementrue_electeur, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
                </div>

                <div class="form-group">
                    <label for="adresse_electeur">Adresse <sup>*</sup> </label>
                    <input type="text" class="form-control" id="adresse_electeur" name="adresse_electeur" placeholder="adresse electeur" value="<?php echo filter_var($this->adresse_electeur, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
                </div>

                <div class="form-group">
                    <label for="adresse2_electeur">Complement d'adresse</label>
                    <input type="text" class="form-control" id="adresse2_electeur" name="adresse2_electeur" placeholder="Complement d'adresse electeur" value="<?php echo filter_var($this->adresse2_electeur, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
                </div>

                <div class="form-group">
                    <label for="cp_electeur">code postal <sup>*</sup></label>
                    <input type="number" class="form-control" id="cp_electeur" name="cp_electeur" placeholder="Code postal" value="<?php echo filter_var($this->cp_electeur, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
                </div>

                <div class="form-group">
                    <label for="pays_electeur">Pays <sup>*</sup> </label>
                    <select class="form-control" id="pays_electeur" name="pays_electeur" onChange="getVille(this.value)">
                        <option value="" selected>--- Choisir un pays ---</option> 
                        <?php foreach ($this->country_states as $key => $value) : ?>
                            <option value="<?php echo $key ?>" ><?php echo $value["name"] ?></option> 
                        <?php endforeach; ?> 
                    </select>
                </div>

                <div class="form-group">
                    <label for="ville_electeur">Ville <sup>*</sup></label>
                    <select class="form-control" id="ville_electeur" name="ville_electeur">
                        <option name="option" value="" selected>--- Choisir une ville ---</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="dt_naissance_electeur">Date de Naissanece <sup>*</sup></label>
                    <input type="date" class="form-control" id="dt_naissance_electeur" name="dt_naissance_electeur" placeholder="date de naissance" value="<?php echo filter_var($this->dt_naissance_electeur, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
                </div>

                <div class="form-group">
                    <label for="tel_electeur">Téléphone fix</label>
                    <input type="number"  class="form-control" id="tel_electeur" name="tel_electeur" placeholder="télephone fix" value="<?php echo filter_var($this->tel_electeur, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
                </div>

                <div class="form-group">
                    <label for="portable_electeur">Téléphone portable</label>
                    <input type="number"  class="form-control" id="portable_electeur" name="portable_electeur" placeholder="Téléphone portable" value="<?php echo filter_var($this->portable_electeur, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
                </div>

                <div class="form-group">
                    <label for="email_electeur">Email <sup>*</sup></label>
                    <input type="text" class="form-control" id="email_electeur" name="email_electeur" placeholder="Email" value="<?php echo filter_var($this->email_electeur, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
                </div>
 
                <div class="form-group">
                    <label for="abstentionniste_electeur">abstentionniste <sup>*</sup></label>
                    <select class="form-control" id="abstentionniste_electeur" name="abstentionniste_electeur">
                        <option value="" selected>--- Choisir abstentionniste ---</option>
                        <option value="0" >Nom</option>
                        <option value="1">Oui</option>
                    </select>
                </div>
 

                <button type="submit" class="btn btn-warning" name="submit" value="submit">Enregistrer</button>
            </form>
        </div> 
    </div>
</div>
</div>


   <!-- JS personaliser --> 
   <script type="text/javascript" src="application\views\electeur-form\electeur-form.js" defer></script>