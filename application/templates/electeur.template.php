<!doctype html>
<html lang="fr">

<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test Entretien NOUNDOU Steve</title>
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="application/resources/bootstrap/css/bootstrap.min.css">
    <!-- CSS personaliser --> 
    <link rel="stylesheet" href="application/resources/main.css">  

    <!--data tables CSS -->
    <link rel="stylesheet" type="text/css" href="application/resources/datatables/datatables.min.css" />
    <!--data tables bootstrap 4 CSS-->
    <link rel="stylesheet" type="text/css" href="application/resources/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

    <!-- font-awesome CSS  -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body >

  <!-- Header -->
    <header class="mb-auto">
            <?php
                previoo\Page::insertModule('Header'); 
            ?>
    </header>
    
  <!-- Body -->
    <div class=" d-flex w-100 h-100 p-3 mx-auto flex-column">
        <?php 
            $this->insertView(); 
        ?>
    </div>


        <!-- footer -->
    <footer class="pt-4 pt-md-5 border-top">
        <?php
            previoo\Page::insertModule('Footer'); 
        ?>
    </footer>

    <!-- jQuery, Popper.js, Bootstrap JS -->
    <script src="application/resources/jquery/jquery-3.3.1.min.js"></script>
    <script src="application/resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="application/resources/popper/popper.min.js"></script>

    
    <!-- data tables JS -->
    <script type="text/javascript" src="application/resources/datatables/datatables.min.js"></script>

    <!-- JS personaliser --> 
    <script type="text/javascript" src="application/resources/main.js" async></script>


</body>

</html>

